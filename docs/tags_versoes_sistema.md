---
id: tags_versoes_sistema
title: Tags de Versões do Sistema
---

Para manter as versões do software, utilizamos o padrão [SemVer (Semantic Versioning)](https://semver.org).

Nesse caso, dada uma versão do software **X.Y.Z**, incremente o número:

 - **X** quando são realizadas alterações que podem causar incompatilibilidades na API. 
 -Ex.: Atualização de um Framework.

 - **Y** quando são adicionadas novas funcionalidades (Features).
 -Ex.: Criação da funcionalidade de autenticação de usuários no sistema.

 - **Z** quando são realizados ajustes/correções (hotfixes/bugfixes).
 -Ex.: A tela de login parou de funcionar e precisa ser corrigida

