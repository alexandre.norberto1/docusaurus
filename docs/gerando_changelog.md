---
id: gerando_changelog
title: Gerando Changelog
---

O CHANGELOG é um arquivo que mantém a descrição das alterações realizadas dentro de cada versão do software. Desse modo, o CHANGELOG é atualizado sempre que o número da versão é incrementado.

Para gerar o arquivo CHANGELOG automaticamente, utilizamos o [standard-version](https://github.com/conventional-changelog/standard-version).

O conteúdo do CHANGELOG gerado é baseado nas mensagens de commit criadas, no número da versão lançada e na sua data de lançamento.

