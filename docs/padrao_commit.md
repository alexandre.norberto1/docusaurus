---
id: padrao_commit
title: Padrão de commit
sidebar_label: Padrão de commit
---

Para auxiliar na padronização das mensagens de **commit** do projeto utilizamos as ferramentas: [**commitlint**](https://commitlint.js.org), [**commitizen**](http://commitizen.github.io/cz-cli/) e [**husky**](https://github.com/typicode/husky).

## Instruções

Na raiz do diretório do projeto digite o comando:

    git commit

Aparecerá uma tela para que você selecione o tipo de alteração que o **commit** representa:

![enter image description here](https://github.com/commitizen/cz-cli/raw/master/meta/screenshots/add-commit.png)

Após selecionar o tipo de alteração, aparecerão algumas questões relativas ao **commit**:

![enter image description here](https://miro.medium.com/max/1950/1*LLg51JFtTDcfJhZ4qxxcDA.png)

Completando o questionário apresentado, aparecerá uma mensagem de sucesso do commit ou algum aviso de erro, caso algo estiver fora do padrão.