---
id: gitflow
title: GitFlow
---

Para mantermos uma organização no fluxo de desenvolvimento do projeto e seu versionamento no repositório, utilizamos o modelo de **branches** publicado e popularizado por [Vincent Driessen em nvie](http://nvie.com/posts/a-successful-git-branching-model/):

![enter image description here](https://nvie.com/img/git-model@2x.png)

Para auxiliar nesse processo, utilizamos o [GitFlow](https://github.com/nvie/gitflow).

A seguir, estão alguns links de materiais sobre o uso do **GitFlow**:

 - https://www.blexin.com/en-US/Article/Blog/Versioning-of-Nuget-packages-with-GitFlow-and-SemVer-41
 - https://fjorgemota.com/git-flow-uma-forma-legal-de-organizar-repositorios-git/
 - https://tableless.com.br/git-flow-introducao/
